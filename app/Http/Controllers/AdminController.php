<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use DB;
use App\Item;
use App\Category;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $itemCount = Item::all()->count();
        $categoryCount = Category::all()->count();
        return view('admin.index', compact('itemCount', 'categoryCount'));
    }

    public function productIndex()
    {
        $allItems = DB::table('items')
            ->join('categories', 'categories.id', '=', 'items.category_id')
            ->select('items.*', 'categories.name as category_name')
            ->get();
        return view('admin.items.index', compact('allItems'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.items.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'bail|required|max:100|min:3',
            'category' => 'bail|required',
            'price' => 'bail|required|min:0',
            'description' => 'bail|required',
            'image1' => 'bail|required|image',
            'image2' => 'bail|required|image',
            'image3' => 'bail|required|image',
            'image4' => 'bail|required|image',
        ]);

        $image1Path = Storage::disk('public')->put("item_images", request('image1'));
        $image2Path = Storage::disk('public')->put("item_images", request('image2'));
        $image3Path = Storage::disk('public')->put("item_images", request('image3'));
        $image4Path = Storage::disk('public')->put("item_images", request('image4'));


        DB::beginTransaction();
        try {
            $items = Item::forcecreate([
                'category_id' => request('category'),
                'name' => request('name'),
                'price' => request('price'),
                'description' => request('description'),
                'image1' => $image1Path,
                'image2' => $image2Path,
                'image3' => $image3Path,
                'image4' => $image4Path
            ]);
            DB::commit();
            session()->flash('success', 'New item has been created');
            return redirect('/admin/dashboard/manage-products/');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('failed', 'Item creation failed. Please try again.');
            return back();
        }
    }

    public function show($id) {
        $item = Item::find($id);
        $category = DB::table('items')
            ->join('categories', 'categories.id', '=', 'items.category_id')
            ->select('categories.name as category_name')
            ->where('items.id', '=', $id)
            ->limit(1)
            ->get();
        return view('admin.items.show', compact('item', 'category'));
    }

    public function edit($id)
    {
        $item = Item::find($id);
        $category = DB::table('items')
            ->join('categories', 'categories.id', '=', 'items.category_id')
            ->select('categories.id', 'categories.name as category_name')
            ->where('items.id', '=', $id)
            ->limit(1)
            ->get();
        $categories = Category::all();
    	return view('admin.items.update', compact('item', 'category', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'name' => 'bail|required|max:100|min:3',
            'category' => 'bail|required',
            'price' => 'bail|required|min:0',
            'description' => 'bail|required',
            'image1' => 'bail|image',
            'image2' => 'bail|image',
            'image3' => 'bail|image',
            'image4' => 'bail|image',
        ]);

        // dd($request);

        // Deleting the old image
        // $deletingFile = News::find($id);
        // Storage::delete('/public/storage/' . $deletingFile->image);
        // $imagePath = Storage::disk('public')->put("news_images", request('image'));

        $images['image1'] = $request->has('image1') ? 1 : 0;
        $images['image2'] = $request->has('image2') ? 1 : 0;
        $images['image3'] = $request->has('image3') ? 1 : 0;
        $images['image4'] = $request->has('image4') ? 1 : 0;

        DB::beginTransaction();
        try {
            if ($images['image1'] == 1)
                $image1Path = Storage::disk('public')->put("item_images", request('image1'));
            if ($images['image2'] == 1)
                $image2Path = Storage::disk('public')->put("item_images", request('image2'));
            if ($images['image3'] == 1)
                $image3Path = Storage::disk('public')->put("item_images", request('image3'));
            if ($images['image4'] == 1)
                $image4Path = Storage::disk('public')->put("item_images", request('image4'));

            DB::table('items')->where('id', '=', $id)->update([
                'category_id' => request('category'),
                'name' => request('name'),
                'price' => request('price'),
                'description' => request('description'),
                'image1' => $images['image1'] == 1 ? $image1Path : request('imageOne'),
                'image2' => $images['image2'] == 1 ? $image2Path : request('imageTwo'),
                'image3' => $images['image3'] == 1 ? $image3Path : request('imageThree'),
                'image4' => $images['image4'] == 1 ? $image4Path : request('imageFour'),
            ]);
            DB::commit();
            session()->flash('success', 'Item has been updated.');
            return redirect('/admin/dashboard/manage-products');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('failed', 'Item update failed. Please try again.');
            return redirect('/admin/dashboard/manage-products');
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('items')->where('id', '=', $id)->delete();
            DB::commit();
            session()->flash('success', 'Item has been deleted');
            return redirect('/admin/dashboard/manage-products');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('failed', 'Item creation failed. Please try again.');
            return redirect('/admin/dashboard/manage-products/');
        }
    }
}
