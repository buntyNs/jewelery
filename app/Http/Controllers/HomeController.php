<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Item;
use App\Category;
class HomeController extends Controller
{


   function getData(){
      $week =  ITEM::select('id','name','price','image1','category_id')->paginate(3);
      $men =  ITEM::select('id','name','price','image1','category_id')->where('category_id',1)->paginate(4);
      $women =  ITEM::select('id','name','price','image1','category_id')->where('category_id',2)->paginate(4);
      return view('home',compact('week','men','women'));
   }

    function getMensProduct(){
       $item =  ITEM::select('id','name','price','image1')->where('category_id',1)->paginate(1);
       return view('product_men',compact('item'));
    // return json_encode($item);
    }

    function getWomenProduct(){
        $item =  ITEM::select('id','name','price','image1')->where('category_id',2)->paginate(1);
        return view('product_women',compact('item'));
     }


   function getRelatedPost($cat){
      $item =  ITEM::select('id','name','price','image1')->where('category_id',$cat)->paginate(4);
      return $item;
     }


     function getMenSingleProduct($id){
        $item =  ITEM::select('id','name','price','description','image1','image2','image3','image4')
                    ->where('category_id',1)
                    ->where('id',$id)
                    ->get();
         $rel=  ITEM::select('id','name','price','image1','category_id')->where('category_id',1)->paginate(4);
        return view('single_product',compact('item','rel'));
     }


     function getwomenSingleProduct($id){
        $item =  ITEM::select('id','name','price','description','image1','image2','image3','image4')
                    ->where('category_id',2)
                    ->where('id',$id)
                    ->get();
      $rel =  ITEM::select('id','name','price','image1','category_id')->where('category_id',2)->paginate(4);
        return view('single_product',compact('item','rel'));
     }



     


}
