<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'name',
        'price',
        'category_id',
        'description',
        'image1',
        'image2',
        'image3',
        'image4'
    ];
}
