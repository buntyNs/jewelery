@extends('master')

@section ('content')
        <!-- Breadcrumb area Start -->
        <section class="page-title-area bg-image ptb--80" data-bg-image="assets/img/bg/page_title_bg.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="page-title">About Us</h1>
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="current"><span>About Us</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb area End -->


        <!-- Main Content Wrapper Start -->
        <main class="main-content-wrapper">
            <div class="inner-page-content pt--75 pt-md--55">
                <!-- Contact Area Start -->
                <section class="contact-area mb--75 mb-md--55">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 mb-sm--30">
                                <div class="heading mb--32">
                                    <h2>Get In Touch</h2>
                                    <hr class="delimeter">
                                </div>
                                <div class="contact-info mb--20">
                                    <p><i class="fa fa-map-marker"></i>221b Baker St, Marylebone <br>London NW1 6XE, UK</p>
                                    <p><i class="fa fa-phone"></i> +1-202-242-8157</p>
                                    <p><i class="fa fa-fax"></i> +1-202-501-1829</p>
                                    <p><i class="fa fa-clock-o"></i> Mon – Fri : 9:00 – 18:00</p>
                                </div>
                                <div class="social">
                                    <a href="https://www.facebook.com" class="social__link">
                                        <i class="la la-facebook"></i>
                                    </a>
                                    <a href="https://www.twitter.com" class="social__link">
                                        <i class="la la-twitter"></i>
                                    </a>
                                    <a href="https://www.plus.google.com" class="social__link">
                                        <i class="la la-google-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-7 offset-lg-1">
                                <div class="heading mb--40">
                                    <h2>About Us</h2>
                                    <hr class="delimeter">
                                </div>
                                <div>
                               <p> Here at BODYION we ensure our customers get the best quality negative ion bracelets in the market. 
                                BODYION employees work everyday with the advanced technology to create the perfect negative ion bracelets. Which always leaves us a step ahead from our existing competitors. 

                                Once you do business with BODYION, you are guaranteed 100% customer satisfaction. This is also why we offer a “limited lifetime warranty” for all of our products. 

                                With BODYION we promise to supply the highest quality product that contain and maintain a negative ion level that create a balance with the health of the customer. 

                                Our entire site is intended for our customers. We are happy to always provide fast and quality service.
                                </p>

                                <p>
                                <h3>Benefits</h3>

                                So what is negative ion ?
                                They are beneficial for the human body while positive ions are harmful. In fact, you will find the highest concentrations of negative ions in natural, clean air. Ions are invisible charged particles in the air – either molecules or atoms, which bear an electric charge. Some particles are positively charged and some are negatively charged. To put it simply, positive ions are molecules that have lost one or more electrons whereas negative ions are actually oxygen atoms with extra-negatively-charged electrons. Negative ions are abundant in nature, especially around waterfalls, on the ocean surf, at the beach, and after a storm. They are widespread in mountains and forests.<br/><br/>

                                Negative ions are present in the air we breathe in and they are also present in our bodies. The degree to which negative ions contribute to overall well-being and health is scientifically proven:<br/><br/>

                                <span> +  </span>Strengthens one's immune system.<br/>
                                +Increases blood circulation<br/>
                                +Raises oxygen levels<br/>
                                +Alleviates physical stress, stiffness, cramps or discomfort<br/>
                                +They balance the autonomic nervous system, promoting deep sleep and healthy digestion.<br/>
                                +Promotes quality sleep<br/><br/>

                                On the other hand, in polluted cities, crowded areas and in confined spaces such as offices, industrial areas, schools and cars, you will find the highest concentration of unhealthy positive ions.<br/><br/>

                                The most important benefit of negative ions is that they clear the air such as pollen, mold spores, bacteria, and viruses. Besides they also clear the air of dust, pet dander and cigarette smoke. Negative ions perform this function by attaching themselves to positively charged particles in large numbers and negatively charging those particles. As a result, these viruses, bacteria, and pollen become too heavy to remain airborne and are thus prevented from entering your breathing passage where they can make you fall sick or affect your immune system
                                                                </p>


                                </div>


                            </div>
                        </div>
                    </div>
                </section>
                <!-- Contact Area End -->

                <!-- Google Map Area Start -->
                <!-- <div class="google-map-area">
                    <div id="google-map"></div>
                </div> -->
                <!-- Google Map Area End -->
            </div>
        </main>
        <!-- Main Content Wrapper End -->


@endsection