@extends('admin.layouts.master')

@section('page-css')
<!-- Waves Effect Css -->
<link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />
@endsection

@section('content')
<?php $sidebar = 'dashboard'; ?>
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>DASHBOARD</h2>
        </div>

        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-blue hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">reorder</i>
                    </div>
                    <div class="content">
                        <div class="text">ALL ITEMS</div>
                        <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">{{ $itemCount }}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-blue hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">library_books</i>
                    </div>
                    <div class="content">
                        <div class="text">ALL CATEGORIES</div>
                        <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">{{ $categoryCount }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
<!-- Select Plugin Js -->
<script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/admin/plugins/node-waves/waves.min.js"></script>

<!-- Custom Js -->
<script src="/admin/js/admin.js"></script>
@endsection
