@extends('admin.layouts.master')

@section('page-css')
<!-- Waves Effect Css -->
<link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

<!-- JQuery DataTable Css -->
<link href="/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css" rel="stylesheet">

<!-- Sweet Alert Css -->
<link href="/admin/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('content')
<?php $sidebar = 'manage_products'; ?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-blue">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li class="active">Manage Products</li>
                </ol>

                <div class="card">
                    <div class="header">
                        <h2>
                            ALL PRODUCTS
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <div class="button-demo">
                                <a href="/admin/dashboard/manage-products/create" type="button" data-color="blue" class="btn bg-blue waves-effect">New Product</a>
                            </div>
                        </ul>
                    </div>

                    <div class="body">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Category</th>
                                    <th>Price <span style="color:green">($)</span></th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($allItems as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->category_name }}</td>
                                    <td>{{ $item->price }}</td>
                                    <td>{{ substr($item->description, 0, 20) }} ...</td>
                                    <td>
                                        <a href="#" delete = {{ $item->id }} class="delete">
                                            <i class="material-icons"><small>delete</small></i>
                                        </a>
                                        <a href="/admin/dashboard/manage-products/edit/{{ $item->id }}">
                                            <i class="material-icons"><small>mode_edit</small></i>
                                        </a>
                                        <a href="/admin/dashboard/manage-products/show/{{ $item->id }}">
                                            <i class="material-icons"><small>info</small></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if ($flash = session('success'))  
            <div class="alert bg-green alert-dismissible text-center" role="alert" style="position:absolute; bottom:5px; right:30px; z-index:20">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ $flash }}
            </div>
        @endif
        @if ($flash = session('failed'))  
            <div class="alert bg-red alert-dismissible text-center" role="alert" style="position:absolute; bottom:5px; right:30px; z-index:20">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ $flash }}
            </div>
        @endif
    </div>
</section>
@endsection

@section('page-scripts')
<!-- Select Plugin Js -->
<script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/admin/plugins/node-waves/waves.min.js"></script>

<!-- Sweet Alert Plugin Js -->
<script src="/admin/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

<!-- Custom Js -->
<script src="/admin/js/admin.js"></script>
<script src="/admin/js/pages/tables/jquery-datatable.js"></script>
<script src="/admin/js/pages/ui/dialogs.js"></script>

<script>
    $(".delete").on("click", function() {
        showCancelMessage(this.getAttribute("delete"), this);
    });

    function showCancelMessage(id, dom) {
        swal({
            title: "Are you sure want to delete this?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "GET",
                    url: "/admin/dashboard/manage-products/delete/" + id,
                    success: function(msg) {
                        swal("Deleted!", "Successfully deleted", "success");
                        $(dom).closest("tr").remove();
                    }
                });
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
        });
    }

    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, 
        function(){
            $(this).remove(); 
        });
    }, 3000);
</script>
@endsection
