@extends('admin.layouts.master')

@section('page-css')
<!-- Waves Effect Css -->
<link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

<!-- Light Gallery Plugin Css -->
<link href="/admin/plugins/light-gallery/css/lightgallery.min.css" rel="stylesheet">
@endsection

@section('content')
<?php $sidebar = 'manage_products'; ?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-blue">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/manage-products">Manage Products</a></li>
                    <li class="active">View</li>
                </ol>

                <div class="card">
                    <div class="body">
                        <h3 class="card-inside-title">Product name</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ ucfirst($item->name) }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Product category</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ ucfirst($category[0]->category_name) }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Product description</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea rows="10" class="form-control no-resize" disabled>{{ ucfirst($item->description) }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Product price<span style="color:green"> ($)</span></h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="{{ $item->price }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                                               
                        <h3 class="card-inside-title">Product images</h3>
                        <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="/storage/{{ $item->image1 }}" data-sub-html="{{ $item->name }}">
                                    <img class="img-responsive thumbnail" src="/storage/{{ $item->image1 }}">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="/storage/{{ $item->image2 }}" data-sub-html="{{ $item->name }}">
                                    <img class="img-responsive thumbnail" src="/storage/{{ $item->image2 }}">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="/storage/{{ $item->image3 }}" data-sub-html="{{ $item->name }}">
                                    <img class="img-responsive thumbnail" src="/storage/{{ $item->image3 }}">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="/storage/{{ $item->image4 }}" data-sub-html="{{ $item->name }}">
                                    <img class="img-responsive thumbnail" src="/storage/{{ $item->image4 }}">
                                </a>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Date of the creation</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $item->created_at->toFormattedDateString() }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
<!-- Select Plugin Js -->
<script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/admin/plugins/node-waves/waves.min.js"></script>

<!-- Light Gallery Plugin Js -->
<script src="/admin/plugins/light-gallery/js/lightgallery-all.min.js"></script>

<!-- Custom Js -->
<script src="/admin/js/pages/medias/image-gallery.js"></script>
<script src="/admin/js/admin.js"></script>
@endsection
