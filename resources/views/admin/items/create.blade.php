@extends('admin.layouts.master')

@section('page-css')
<!-- Waves Effect Css -->
<link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

<!-- Bootstrap Select Css -->
<link href="/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
@endsection

@section('content')
<?php $sidebar = 'manage_products'; ?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-blue">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/manage-products">Manage Products</a></li>
                    <li class="active">Create</li>
                </ol>

                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="/admin/dashboard/manage-products/create" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="name">Product name <span style="color:red">*</span></label>
                                    <input type="text" id="name" class="form-control" name="name" value="{{ old('name') }}" required>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label for="category">Product category <span style="color:red">*</span></label>
                                <select class="form-control show-tick" name="category" required>
                                    <option value="">-- please select --</option>
                                    @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ ucfirst($category->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="price">Product price<span style="color:green"> ($)</span><span style="color:red"> *</span></label>
                                    <input type="text" id="price" class="form-control" name="price" value="{{ old('price') }}" min="0" required>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="description">Product description <span style="color:red">*</span></label><br><br>
                                    <textarea id="description" class="form-control" rows="3" minlength="5" name="description" required>{{ old('description') }}</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label for="image1">Upload images of the product <span style="color:red">*</span></label>
                                        <input type="file" name="image1" accept="image/x-png,image/gif,image/jpeg" onchange="readURL0(this);" style="padding: 8px 0px 8px 0px" required>
                                        <img id="first-image" src="https://via.placeholder.com/150" alt="Image one" style="max-width:180px;" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label for="image2">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        <input type="file" name="image2" accept="image/x-png,image/gif,image/jpeg" onchange="readURL1(this);" style="padding: 8px 0px 8px 0px" required>
                                        <img id="second-image" src="https://via.placeholder.com/150" alt="Image two" style="max-width:180px;" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label for="image3">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        <input type="file" name="image3" accept="image/x-png,image/gif,image/jpeg" onchange="readURL2(this);" style="padding: 8px 0px 8px 0px" required>
                                        <img id="third-image" src="https://via.placeholder.com/150" alt="Image three" style="max-width:180px;" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label for="image4">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        <input type="file" name="image4" accept="image/x-png,image/gif,image/jpeg" onchange="readURL3(this);" style="padding: 8px 0px 8px 0px" required>
                                        <img id="fourth-image" src="https://via.placeholder.com/150" alt="Image four" style="max-width:180px;" />
                                    </div>
                                </div>
                            </div>
                            
                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>

                        @include('admin.layouts.errors')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
<!-- Select Plugin Js -->
<script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/admin/plugins/node-waves/waves.min.js"></script>

<!-- Jquery Validation Plugin Css -->
<script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>

<!-- JQuery Steps Plugin Js -->
<script src="/admin/plugins/jquery-steps/jquery.steps.min.js"></script>

<!-- Sweet Alert Plugin Js -->
<script src="/admin/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Custom Js -->
<script src="/admin/js/admin.js"></script>
<script src="/admin/js/pages/forms/form-validation.js"></script>
<script src="/admin/js/pages/forms/basic-form-elements.js"></script>

<script>
    function readURL0(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#first-image')
                .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#second-image')
                .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#third-image')
                .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#fourth-image')
                .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection
