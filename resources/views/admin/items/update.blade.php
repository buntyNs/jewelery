@extends('admin.layouts.master')

@section('page-css')
<!-- Waves Effect Css -->
<link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

<!-- Bootstrap Select Css -->
<link href="/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<!-- Light Gallery Plugin Css -->
<link href="/admin/plugins/light-gallery/css/lightgallery.min.css" rel="stylesheet">
@endsection

@section('content')
<?php $sidebar = 'manage_products'; ?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-blue">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/manage-products">Manage Products</a></li>
                    <li class="active">Update</li>
                </ol>

                <form id="form_advanced_validation" action="/admin/dashboard/manage-products/update/{{ $item->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="body">
                            <h3 class="card-inside-title">Product name <span style="color:red">*</span></h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line disabled">
                                            <input type="text" name="name" id="name" class="form-control" value="{{ ucfirst($item->name) }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="card-inside-title">Product category <span style="color:red">*</span></h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line disabled">
                                            <select class="form-control show-tick" name="category" required>
                                                @foreach ($categories as $category)
                                                <option value="{{ $category->id }}" @if (isset($category[0]->id)) selected @endif>{{ ucfirst($category->name) }}</option>                      
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="card-inside-title">Product price <span style="color:green"> ($)</span><span style="color:red"> *</span></h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line disabled">
                                            <input type="text" name="price" id="price" class="form-control" value="{{ $item->price }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="card-inside-title">Product description <span style="color:red">*</span></h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line disabled">
                                            <textarea id="description" class="form-control" rows="3" minlength="5" name="description" required>{{ ucfirst($item->description) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                
                            <h3 class="card-inside-title">Update product images <span style="color:red">*</span></h3>
                            <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group form-float">
                                            <label for="image1">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <input type="file" name="image1" accept="image/x-png,image/gif,image/jpeg" onchange="readURL0(this);" style="padding: 8px 0px 8px 0px">
                                            <img id="first-image" src="/storage/{{ $item->image1 }}" alt="{{ $item->name }}" style="max-width:180px;" />
                                            <input type="hidden" name="imageOne" value="{{ $item->image1 }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-float">
                                            <label for="image2">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <input type="file" name="image2" accept="image/x-png,image/gif,image/jpeg" onchange="readURL1(this);" style="padding: 8px 0px 8px 0px">
                                            <img id="second-image" src="/storage/{{ $item->image2 }}" alt="{{ $item->name }}" style="max-width:180px;" />
                                            <input type="hidden" name="imageTwo" value="{{ $item->image2 }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-float">
                                            <label for="image3">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <input type="file" name="image3" accept="image/x-png,image/gif,image/jpeg" onchange="readURL2(this);" style="padding: 8px 0px 8px 0px">
                                            <img id="third-image" src="/storage/{{ $item->image3 }}" alt="{{ $item->name }}" style="max-width:180px;" />
                                            <input type="hidden" name="imageThree" value="{{ $item->image3 }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-float">
                                            <label for="image4">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <input type="file" name="image4" accept="image/x-png,image/gif,image/jpeg" onchange="readURL3(this);" style="padding: 8px 0px 8px 0px">
                                            <img id="fourth-image" src="/storage/{{ $item->image4 }}" alt="{{ $item->name }}" style="max-width:180px;" />
                                            <input type="hidden" name="imageFour" value="{{ $item->image4 }}">
                                        </div>
                                    </div>
                                </div>

                                @include('admin.layouts.errors')
                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </div>
                    </div>
                </form>    
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="/admin/plugins/jquery-steps/jquery.steps.min.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="/admin/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Light Gallery Plugin Js -->
    <script src="/admin/plugins/light-gallery/js/lightgallery-all.min.js"></script>

    <!-- Ckeditor -->
    <script src="/admin/plugins/ckeditor/ckeditor.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/pages/medias/image-gallery.js"></script>
    <script src="/admin/js/admin.js"></script>
    <script src="/admin/js/pages/forms/form-validation.js"></script>
    <script src="/admin/js/pages/forms/editors.js"></script>
    <script src="/admin/js/pages/forms/basic-form-elements.js"></script>

    <script>
        function readURL0(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                    $('#first-image')
                    .attr('src', e.target.result);
                };
    
                reader.readAsDataURL(input.files[0]);
            }
        }
    
        function readURL1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                    $('#second-image')
                    .attr('src', e.target.result);
                };
    
                reader.readAsDataURL(input.files[0]);
            }
        }
    
        function readURL2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                    $('#third-image')
                    .attr('src', e.target.result);
                };
    
                reader.readAsDataURL(input.files[0]);
            }
        }
    
        function readURL3(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                    $('#fourth-image')
                    .attr('src', e.target.result);
                };
    
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
