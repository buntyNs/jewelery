<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li @if ($sidebar == 'dashboard') class="active" @endif>
                    <a href="/admin/dashboard">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li @if ($sidebar == 'manage_products') class="active" @endif>
                    <a href="/admin/dashboard/manage-products">
                        <i class="material-icons">layers</i>
                        <span>Manage Products</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; {{ now()->year }} <a href="javascript:void(0);">All Rights Reserved</a>
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
