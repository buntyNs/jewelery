<?php

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

//home
Route::get('/', 'HomeController@getData');

//all products
Route::get('/product/men', 'HomeController@getMensProduct');
Route::get('/product/women', 'HomeController@getWomenProduct');

//single product
Route::get('/product/men/{id}', 'HomeController@getMenSingleProduct');
Route::get('/product/women/{id}', 'HomeController@getWomenSingleProduct');

// Admin routes
Route::get('/admin/login', 'SessionController@create')->name('login');
Route::post('/admin/login', 'SessionController@store');
Route::get('/admin/logout', 'SessionController@destroy');

Route::get('/admin/dashboard', 'AdminController@index');

Route::get('/admin/dashboard/manage-products', 'AdminController@productIndex');
Route::get('/admin/dashboard/manage-products/create', 'AdminController@create');
Route::post('/admin/dashboard/manage-products/create', 'AdminController@store');
Route::get('/admin/dashboard/manage-products/show/{id}', 'AdminController@show');
Route::get('/admin/dashboard/manage-products/edit/{id}', 'AdminController@edit');
Route::post('/admin/dashboard/manage-products/update/{id}', 'AdminController@update');
Route::get('/admin/dashboard/manage-products/delete/{id}', 'AdminController@destroy');
